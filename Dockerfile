FROM alpine:edge

RUN apk add --no-cache gettext curl

RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing kubectl
